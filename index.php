<?php include('assets/partials/header.php')  ?> 
<main>
 <!-- hero slider area start -->
 <section class="slider-area">
  <div class="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">
    <div class="hero-single-slide hero-overlay">
      <div class="hero-slider-item bg-img" data-bg="assets/img/eco/2.jpg">
       <div class="slider-text">
         <div class="container">
          <h4 class="mb-3 text-white"> Are You Planning To Join With Us.?</h4>
          <div class="row">
            <div class="col-md-3">
             <div class="form-group has-search">
              <span class="fa fa-map-marker form-control-feedback"></span>
              <input type="text" class="form-control" placeholder="Where Are You Going ?">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group has-search">
              <span class="fa fa-calendar form-control-feedback"></span>
              <input type="date" class="form-control" placeholder="Check-In Date">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group has-search">
              <span class="fa fa-calendar form-control-feedback"></span>
              <input type="date" class="form-control" placeholder="Check-Out Date">
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group has-search">
              <span class="fa fa-user form-control-feedback"></span>
              <input type="number" class="form-control" placeholder="No. Guest">
            </div>
          </div>
          <div class="col-md-1">
            <a href="#" class="btn-style-one"><i class="fa fa-search"></i></a>

          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</div>

</div>
</section>
<!-- hero slider area end -->


<section class="section-padding">
  <div class="container">
   <div class="row">
    <div class="col-md-6 eco-about">
     <h4 class="heading-eco text-dark-color">
      Casa Deyra Private Limited
    </h4>
    <p>
    An exclusive collection of fully furnished serviced apartments and bed & breakfasts well spread across Kathmandu valley. We welcome valued guests from all around the world for short, mid and long term stays. We are currently managing over 30 accommodations consisting of 1BR, 2BR and 3BR units.</p>

    <p>Aesthetically designed inclusive of complete amenities and facilities, we assure you that our accommodations will make you feel “Home away from Home”.</p>

    <div class="row">
      <div class="col-12">
       <div class="eco-description">
        <div class="eco-icon">
         <i class="fa fa-hand-peace-o"></i>
       </div>
       <div class="eco-text">
         <h5><a href="#">HASSEL FREE STAY</a></h5>
         <div>No extra payment will be charged for the estimation. Our expert team members will analyze.</div>
       </div>
     </div>
   </div>
   <div class="col-12">
     <div class="eco-description">
      <div class="eco-icon">
       <i class="fa fa-lock"></i>
     </div>
     <div class="eco-text">
       <h5><a href="#">PRIVACY</a></h5>
       <div>We ensure our customer to deliver the quality work. Get guaranteed customer satisfaction </div>
     </div>
   </div>
 </div>
 <div class="col-12">
   <div class="eco-description">
    <div class="eco-icon">
     <i class="fa fa-map-marker"></i>
   </div>
   <div class="eco-text">
     <h5><a href="#">LOCATION</a></h5>
     <div>Get your job done within our estimated time frame. We will deliver the quality material and expert team members </div>
   </div>
 </div>
</div>
</div>
</div>
<div class="col-md-6">
 <img src="assets/img/eco/abt-img.jpg" alt="" class="img-fluid">
</div>
</div>
</div>
</section> 


<section class="section-padding">
  <div class="container">
   <div class="col-md-12 mb-3">
    <h5 class="eco-heading text-dark-color">Ideas for Kathmandu</h5>
    <span class="eco-line"></span>
    <h2 class="eco-subheading">
     Get inspiration based on what other travelers search for 
   </h2>
 </div>

 <div class="service-content-wrapper">
  <div class="service-carousel-active">
    <div class="service-content">
      <a href="#">
        <article class="eco-product text-center">
         <figure>
          <img src="assets/img/eco/p1.jpg" alt="product" class="img-fluid">
          <span class="fa fa-map-marker"></span>
        </figure>
        <h3>
          Homes With Wifi
          <br>
        </h3>
      </article>
    </a>
  </div>
  <div class="service-content">
    <a href="#">
      <article class="eco-product text-center">
       <figure>
        <img src="assets/img/eco/p2.jpg" alt="product" class="img-fluid">
        <span class="fa fa-map-marker"></span>
      </figure>
      <h3>
        Homes Downtown
        <br>
      </h3>
    </article>
  </a>
</div>
<div class="service-content">
  <a href="#">
    <article class="eco-product text-center">
     <figure>
      <img src="assets/img/eco/p3.jpg" alt="product" class="img-fluid">
      <span class="fa fa-map-marker"></span>
    </figure>
    <h3>
      Homes With Great Families
      <br>
    </h3>
  </article>
</a>
</div>
<div class="service-content">
  <a href="#">
    <article class="eco-product text-center">
     <figure>
      <img src="assets/img/eco/p2.jpg" alt="product" class="img-fluid">
      <span class="fa fa-map-marker"></span>
    </figure>
    <h3>
      Homes With Apartment
      <br>
    </h3>
  </article>
</a>
</div>
<div class="service-content">
  <a href="#">
    <article class="eco-product text-center">
     <figure>
      <img src="assets/img/eco/p1.jpg" alt="product" class="img-fluid">
      <span class="fa fa-map-marker"></span>
    </figure>
    <h3>
      Homes Studio
      <br>
    </h3>
  </article>
</a>
</div>

</div>
</div>

</div>
</section>

<section>
  <div class="container">
    <div class="alert alert-bordered" role="alert">
     <i class="fa fa-info"></i> Travel safe: Be sure to follow any government safety guidelines for travel. <a href=""> Learn more...</a>
   </div>
 </div>
</section>

<section class="section-padding">
  <div class="container">
   <div class="col-md-12 mb-3">
    <h4 class="eco-heading text-dark-color">Find spaces that suit your style</h4>
    <span class="eco-line"></span>
    <h2 class="eco-subheading">
     Properties in Kathmandu average 4.1 out of 5 stars.
   </h2>
 </div>
 <div class="service-content-wrapper">
  <div class="featured-carousel-active">
    <div class="service-content">
      <a href="#">
        <article class="eco-product">
         <figure>
          <img src="assets/img/eco/p1.jpg" alt="product" class="img-fluid">
          <span class="fa fa-map-marker"></span>
        </figure>
        <h3>
          Apartments
          
        </h3>
        <p>View 30+ items</p>
      </article>
    </a>
  </div>
  <div class="service-content">
    <a href="#">
      <article class="eco-product ">
       <figure>
        <img src="assets/img/eco/p2.jpg" alt="product" class="img-fluid">
        <span class="fa fa-map-marker"></span>
      </figure>
      <h3>
       Houses

     </h3>
     <p>View 30+ items</p>
   </article>
 </a>
</div>
<div class="service-content">
  <a href="#">
    <article class="eco-product">
     <figure>
      <img src="assets/img/eco/p3.jpg" alt="product" class="img-fluid">
      <span class="fa fa-map-marker"></span>
    </figure>
    <h3>
      Cottages
      
    </h3>
    <p>View 30+ items</p>
  </article>
</a>
</div>
<div class="service-content">
  <a href="#">
    <article class="eco-product ">
     <figure>
      <img src="assets/img/eco/p2.jpg" alt="product" class="img-fluid">
      <span class="fa fa-map-marker"></span>
    </figure>
    <h3>
      Studio
    </h3>
    <p>View 30+ items</p>
  </article>
</a>
</div>
<div class="service-content">
  <a href="#">
    <article class="eco-product ">
     <figure>
      <img src="assets/img/eco/p1.jpg" alt="product" class="img-fluid">
      <span class="fa fa-map-marker"></span>
    </figure>
    <h3>
      Bunglow
    </h3>
    <p>View 30+ items</p>
  </article>
</a>
</div>

</div>
</div>
</div>
</div>
</section>

<section class="bg-theme">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <article class=" text-center">
         <figure>
          <i class="fa fa-map fa-3x text-white mb-3"></i>
        </figure>
        <h4 class="text-white">
          Explore
        </h4>
        <p class="text-white">Find the perfect place from the worlds best selection of vacation rentals.</p>
      </article>
    </div>

    <div class="col-md-4">
        <article class=" text-center">
         <figure>
          <i class="fa fa-edit fa-3x text-white mb-3"></i>
        </figure>
        <h4 class="text-white">
          Book
        </h4>
        <p class="text-white">Safer and more secure when you pay and book online with us.</p>
      </article>
    </div>

    <div class="col-md-4">
        <article class=" text-center">
         <figure>
          <i class="fa fa-suitcase fa-3x text-white mb-3"></i>
        </figure>
        <h4 class="text-white">
          Enjoy
        </h4>
        <p class="text-white">Enjoy more space and more privacy of an entire vacation home.</p>
      </article>
    </div>

   

  </div>
</div>
</section>

<section class="section-padding">
  <div class="container">
   <div class="col-md-12 mb-3">
    <h4 class="eco-heading text-dark-color">Find the best place to stay — Major Cities</h4>
    <span class="eco-line"></span>
    <h2 class="eco-subheading">
     Properties in Kathmandu & Pokhara average 4.1 out of 5 stars.
   </h2>
 </div>
 <div class="service-content-wrapper">
  <div class="featured-carousel-active">
    <div class="service-content">
      <a href="#">
        <article class="eco-product">
         <figure>
          <img src="assets/img/eco/p1.jpg" alt="product" class="img-fluid">
          <span class="fa fa-map-marker"></span>
        </figure>
        <h3>
          Apartments
          
        </h3>
       <ul class="list-inline fea">
        <li class="list-inline-item">2 Bedroom </li>
        <li class="list-inline-item">1 Bathroom</li>
        <li class="list-inline-item">1 Balcony</li>
      </ul> 
       <ul class="list-inline">
        <li class="list-inline-item">$212 <small>avg/night</small></li>
        <li class="list-inline-item">
           <div class="star-rating">
        <span class="fa fa-star-o" data-rating="1"></span>
        <span class="fa fa-star-o" data-rating="2"></span>
        <span class="fa fa-star-o" data-rating="3"></span>
        <span class="fa fa-star-o" data-rating="4"></span>
        <span class="fa fa-star-o" data-rating="5"></span>
        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
      </div>
        </li>
      </ul> 
      </article>
    </a>
  </div>
  <div class="service-content">
    <a href="#">
      <article class="eco-product ">
       <figure>
        <img src="assets/img/eco/p2.jpg" alt="product" class="img-fluid">
        <span class="fa fa-map-marker"></span>
      </figure>
      <h3>
       Houses

     </h3>
    <ul class="list-inline fea">
        <li class="list-inline-item">2 Bedroom </li>
        <li class="list-inline-item">1 Bathroom</li>
        <li class="list-inline-item">1 Balcony</li>
      </ul> 
       <ul class="list-inline">
        <li class="list-inline-item">$212 <small>avg/night</small></li>
        <li class="list-inline-item">
           <div class="star-rating">
        <span class="fa fa-star-o" data-rating="1"></span>
        <span class="fa fa-star-o" data-rating="2"></span>
        <span class="fa fa-star-o" data-rating="3"></span>
        <span class="fa fa-star-o" data-rating="4"></span>
        <span class="fa fa-star-o" data-rating="5"></span>
        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
      </div>
        </li>
      </ul>
   </article>
 </a>
</div>
<div class="service-content">
  <a href="#">
    <article class="eco-product">
     <figure>
      <img src="assets/img/eco/p3.jpg" alt="product" class="img-fluid">
      <span class="fa fa-map-marker"></span>
    </figure>
    <h3>
      Cottages
      
    </h3>
    <ul class="list-inline fea">
        <li class="list-inline-item">2 Bedroom </li>
        <li class="list-inline-item">1 Bathroom</li>
        <li class="list-inline-item">1 Balcony</li>
      </ul> 
       <ul class="list-inline">
        <li class="list-inline-item">$212 <small>avg/night</small></li>
        <li class="list-inline-item">
           <div class="star-rating">
        <span class="fa fa-star-o" data-rating="1"></span>
        <span class="fa fa-star-o" data-rating="2"></span>
        <span class="fa fa-star-o" data-rating="3"></span>
        <span class="fa fa-star-o" data-rating="4"></span>
        <span class="fa fa-star-o" data-rating="5"></span>
        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
      </div>
        </li>
      </ul>
  </article>
</a>
</div>
<div class="service-content">
  <a href="#">
    <article class="eco-product ">
     <figure>
      <img src="assets/img/eco/p2.jpg" alt="product" class="img-fluid">
      <span class="fa fa-map-marker"></span>
    </figure>
    <h3>
      Studio
    </h3>
   <ul class="list-inline fea">
        <li class="list-inline-item">2 Bedroom </li>
        <li class="list-inline-item">1 Bathroom</li>
        <li class="list-inline-item">1 Balcony</li>
      </ul> 
       <ul class="list-inline">
        <li class="list-inline-item">$212 <small>avg/night</small></li>
        <li class="list-inline-item">
           <div class="star-rating">
        <span class="fa fa-star-o" data-rating="1"></span>
        <span class="fa fa-star-o" data-rating="2"></span>
        <span class="fa fa-star-o" data-rating="3"></span>
        <span class="fa fa-star-o" data-rating="4"></span>
        <span class="fa fa-star-o" data-rating="5"></span>
        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
      </div>
        </li>
      </ul>
  </article>
</a>
</div>
<div class="service-content">
  <a href="#">
    <article class="eco-product ">
     <figure>
      <img src="assets/img/eco/p1.jpg" alt="product" class="img-fluid">
      <span class="fa fa-map-marker"></span>
    </figure>
    <h3>
      Bunglow
    </h3>
    <ul class="list-inline fea">
        <li class="list-inline-item">2 Bedroom </li>
        <li class="list-inline-item">1 Bathroom</li>
        <li class="list-inline-item">1 Balcony</li>
      </ul> 
       <ul class="list-inline">
        <li class="list-inline-item">$212 <small>avg/night</small></li>
        <li class="list-inline-item">
           <div class="star-rating">
        <span class="fa fa-star-o" data-rating="1"></span>
        <span class="fa fa-star-o" data-rating="2"></span>
        <span class="fa fa-star-o" data-rating="3"></span>
        <span class="fa fa-star-o" data-rating="4"></span>
        <span class="fa fa-star-o" data-rating="5"></span>
        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
      </div>
        </li>
      </ul>
  </article>
</a>
</div>

</div>
</div>
</div>
</div>
</section>
<section class="map ">

  <div class="container">
     <div class="col-md-12 mb-3">
    <h4 class="eco-heading text-dark-color">Find the best place to stay — Major Cities</h4>
    <span class="eco-line"></span>
    <h2 class="eco-subheading">
     Properties in Kathmandu & Pokhara average 4.1 out of 5 stars.
   </h2>
 </div>
    <iframe class="maps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23762.15497184609!2d85.30866826061526!3d27.7090886277295!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb198a307baabf%3A0xb5137c1bf18db1ea!2sKathmandu%2044600!5e0!3m2!1sen!2snp!4v1595740321927!5m2!1sen!2snp" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
  </div>
</section>
<section class="testimonial-area section-padding bg-img bg-light" >
  <div class="container"> 
    <div class="col-md-12 text-center mb-5">
      <h4 class="eco-heading text-dark-color">What Our Clients Say.?</h4>
      <span class="eco-line"></span>
      <h2 class="eco-subheading">
       The service provided by the Casa Deyra has proven to be the best in quality and durability. 
     </h2>
   </div>                
   <div class="row">
    <div class="col-12">
      <div class="testimonial-thumb-wrapper">
        <div class="testimonial-thumb-carousel">
          <div class="testimonial-thumb">
            <img src="assets/img/testimonial/1.jpg" alt="testimonial-thumb">
          </div>
          <div class="testimonial-thumb">
            <img src="assets/img/testimonial/2.jpg" alt="testimonial-thumb">
          </div>
          <div class="testimonial-thumb">
            <img src="assets/img/testimonial/3.jpg" alt="testimonial-thumb">
          </div>
          
        </div>
      </div>
      <div class="testimonial-content-wrapper">
        <div class="testimonial-content-carousel">
          <div class="testimonial-content">
            <p>Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque</p>
            <div class="ratings">
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
            </div>
            <h5 class="testimonial-author">lindsy niloms</h5>
          </div>
          <div class="testimonial-content">
            <p>Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque</p>
            <div class="ratings">
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
            </div>
            <h5 class="testimonial-author">Daisy Millan</h5>
          </div>
          <div class="testimonial-content">
            <p>Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque</p>
            <div class="ratings">
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
            </div>
            <h5 class="testimonial-author">Anamika lusy</h5>
          </div>
          <div class="testimonial-content">
            <p>Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque</p>
            <div class="ratings">
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
              <span><i class="fa fa-star-o"></i></span>
            </div>
            <h5 class="testimonial-author">Maria Mora</h5>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>


</main>
<?php include('assets/partials/footer.php')  ?>