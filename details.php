<?php include('assets/partials/header.php')  ?> 
<main>


 <!-- hero slider area start -->
 <section class="slider-area container">
  <div class="row">
    <div class="col-md-8">
       <div class="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">

 <div class="hero-single-slide hero-overlay">
  <div class="hero-slider-item bg-img" data-bg="assets/img/eco/3.jpg">
    <div class="slider-text">
      <h3>GOLDEN HOUR BREEZY 1BR APARTMENT @THAMEL</h3>
      <div>
       <p>This 3BHK apartment located in Satdobato height, blocks away from the bustling city offers highly comfortable accommodation; has all the comforts of home for short- and long-term stays.</p>
       <a href="#" class="btn-style-one">Book Nows</a>
     </div>

   </div>
 </div>
</div>

<div class="hero-single-slide hero-overlay">
  <div class="hero-slider-item bg-img" data-bg="assets/img/eco/4.jpg">
    <div class="slider-text">
      <h3>GOLDEN HOUR BREEZY 1BR APARTMENT @THAMEL</h3>
      <div>
       <p>This 3BHK apartment located in Satdobato height, blocks away from the bustling city offers highly comfortable accommodation; has all the comforts of home for short- and long-term stays.</p>
       <a href="#" class="btn-style-one">Book Nows</a>
     </div>

   </div>
 </div>
</div>
</div>
    </div>
    <div class="col-md-4 mt-2">
      <h4 class="text-dark-color">
      Apartment Amenities
    </h4>
     <div class="eco-service-info room-info mt-2">
       <p>
        The service provided by the Casa Deyra has proven to be the best in quality and durability.
      </p>
      <ul>
       <li><i class="fa fa-tv"></i> Electricity Back Up</li>
       <li><i class="fa fa-wifi"></i> High Speed Internet</li>
       <li><i class="fa fa-bath"></i> Basic Toiletries</li>
       <li><i class="fa fa-tv"></i> LED TV</li>
       <li><i class="fa fa-newspaper-o"></i> Newspaper</li>
     </ul>
   </div>
    </div>

  </div>
 
</section>
<!-- hero slider area end -->
<section class="section-padding p-0">
  <div class="container">
   <div class="row">
    <div class="col-md-12 eco-about">
    <p>
    One bedroom apartment with spacious living room and an open kitchen/dining space. Apartment is perfect for those looking to stay long term. Tiny little corner in your bedroom perfectly utilized as a work space. Wardrobe big enough to fit all your clothing. Private balcony is attached to bedroom to enjoy the breeze outside.
    </p>

    <p><b>COVID-19 Measures:</b> For the safety and health of all our guests and on site staffs we are taking precautionary measures like disinfecting apartments and keeping it vacant for 48 hours after every check out, comprehensive staff training, use of hand sanitization, preference to cashless transaction and travel history details for past one month at the time of check in.</p>

    
</div>
</div>
</section>

<section class="section-padding p-0">
  <div class="container">
   <div class="row">
    <div class="col-md-8 eco-about">
      <div class="eco-description">
        <div class="eco-icon">
         <i class="fa fa-map-marker"></i>
       </div>
       <div class="eco-text">
         <h5><a href="#">Location</a></h5>
       </div>
     </div>

    <p>Minutes walk away from core Thamel, located in Dhobichaur, Chhetrapati, Thamel </p>
     <div class="eco-service-info loc-info">
      <ul>
       <li><i class="fa fa-check"></i> 7km from International Airport</li>
       <li><i class="fa fa-check"></i> 1.2km from Garden of Dreams</li>
       <li><i class="fa fa-check"></i> Breakfast</li>
       <li><i class="fa fa-check"></i> 1.2km from Kathmandu Durbar Square</li>
       <li><i class="fa fa-check"></i> 7km away from Patan Durbar Square</li>
       <li><i class="fa fa-check"></i> Taxis available all the time nearby apartment</li>
     </ul>
   </div>
    </div>
<div class="col-md-4 eco-about mb-3">
   <h4 class="text-white">
      Price starts from *
    </h4>
     <div class="eco-service-info price-info mt-2">
      <ul>
       <li>Short (Less than a week)  <span>$45/day</span></li>
       <li>Medium (1-4 weeks) <span>$240/week</span></li>
       <li>Long (a month and more)  <span>$800/month</span></li>
     </ul>
            <a href="#" class="btn-style-one mt-3">Book Nows</a>

   </div>
</div>
    
</div>
</div>
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7064.279312782925!2d85.30643!3d27.712974000000003!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xda34408d3bed9169!2sThamel%20Apartments%20by%20Casa%20Deyra!5e0!3m2!1sen!2sus!4v1595338568235!5m2!1sen!2sus" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</section>
</main>

<?php include('assets/partials/footer.php')  ?> 